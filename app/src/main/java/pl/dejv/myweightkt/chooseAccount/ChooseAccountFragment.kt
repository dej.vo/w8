package pl.dejv.myweightkt.chooseAccount

import android.Manifest
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.choose_account.*
import pl.dejv.myweightkt.R
import pl.dejv.myweightkt.core.common.AbstractFragment
import pl.dejv.myweightkt.core.common.MyWeightInterface
import pub.devrel.easypermissions.EasyPermissions


class ChooseAccountFragment : AbstractFragment(), EasyPermissions.PermissionCallbacks {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.choose_account, container, false)
        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>?) {
        // FIXME
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>?) {
        when (requestCode) {
            MyWeightInterface.GOOGLE_ACCOUNT_REQUEST_CODE -> checkNeededServices()
        }
    }

    private fun checkNeededServices() {
        // FIXME
    }

    private fun setupView() {
        chooseAccount.setOnClickListener {
            chooseGoogleAccount()
        }
    }

    private fun chooseGoogleAccount() {
        activity?.let {
            if (EasyPermissions.hasPermissions(activity!!, Manifest.permission.GET_ACCOUNTS)) {
                // FIXME
            } else {
                EasyPermissions.requestPermissions(activity!!,
                        "This app needs to access Your Google account",
                        MyWeightInterface.GOOGLE_ACCOUNT_REQUEST_CODE,
                        Manifest.permission.GET_ACCOUNTS)
            }
        }
    }

}
