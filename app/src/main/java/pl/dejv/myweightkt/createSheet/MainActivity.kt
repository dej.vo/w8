package pl.dejv.myweightkt.createSheet

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import pl.dejv.myweightkt.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme_MainActivity)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
