package pl.dejv.myweightkt.core.executor.impl

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers

interface PostThreadExecutor {
    fun getScheduler(): Scheduler
}

class UIThread : PostThreadExecutor {

    override fun getScheduler(): Scheduler {
        return AndroidSchedulers.mainThread()
    }
}