package pl.dejv.myweightkt.core.executor.impl

import java.util.concurrent.*

interface ThreadExecutor : Executor

class WorkerExecutor internal constructor(private val threadPoolExecutor: ThreadPoolExecutor =
                                                  ThreadPoolExecutor(3, 5, 10,
                                                          TimeUnit.SECONDS,
                                                          LinkedBlockingQueue<Runnable>(),
                                                          Executors.defaultThreadFactory()))
    : ThreadExecutor {

    override fun execute(command: Runnable?) {
        threadPoolExecutor.execute(command)
    }
}